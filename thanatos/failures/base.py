from functools import partial
from threading import Timer, Lock

from flask import logging

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger(__name__)


class SimpleScheduler(object):
    """
    Simple wrapper for Timer.
    """
    def __init__(self):
        self._utid = 1
        self._utid_lock = Lock()

        self.__timers = {}

    def schedule(self, delay, func, *args, **kwargs):
        with self._utid_lock:
            tid = self._utid
            self._utid += 1

        t = Timer(delay, partial(self.__caller, tid, func, *args, **kwargs))
        self.__timers[tid] = t
        t.start()

    def __caller(self, tid, func, *args, **kwargs):
        if tid in self.__timers:
            del self.__timers[tid]
        func(*args, **kwargs)

    def close(self):
        for t in self.__timers:
            t.cancel()
        self.__timers = {}


class Failure(object):

    _scheduler = SimpleScheduler()

    def trigger(self):
        raise NotImplementedError()
