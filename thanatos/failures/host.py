import logging
import os
import shlex
import subprocess as sp
import psutil
from multiprocessing import cpu_count
from stat import ST_DEV

from thanatos.deps.libvirt import LibvirtConnector

from thanatos.failures.base import Failure


__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

logger = logging.getLogger(__name__)


TestFileName = 'btest_disk_load_file.dat'
TestFileSize = ('10M', 10)


# noinspection PyAbstractClass
class HostFailure(Failure):
    pass


class HostCrash(HostFailure):

    _CoreProcesses ={
        'collectd',
        'redis-server'
    }

    _CoreServices = {
        'collectd',
        # 'redis'
    }

    def __init__(self, duration=0):
        super().__init__()
        self._duration = duration

    def trigger(self):
        # get list of instances
        with LibvirtConnector() as conn:
            instance_list = conn.list_instances()
            logger.debug("Instances to crash: %s", [instance.name for instance in instance_list])

            if instance_list:
                for instance in instance_list:
                    if 'client-' not in instance.name:
                        logger.info("Crashing instance: %s", instance.name)

                        self._scheduler.schedule(
                            0,
                            instance.abrupt_shutdown
                        )
            if self._duration:
                self._scheduler.schedule(
                    self._duration,
                    self.recover
                )

        for service in self._CoreServices:
            # PYTHON 3.5 ONLY - proc = sp.run(['sudo', 'systemctl', 'stop', service], stdout=sp.PIPE)
            p = sp.Popen(['sudo', 'systemctl', 'stop', service], stdout=sp.PIPE, stderr=sp.PIPE)
            out, err = p.communicate()
            rc = p.returncode
            logger.info("\tret=%s out: %s, err: %s", rc, out.decode('utf-8'), err.decode('utf-8'))

    def recover(self):
        for service in self._CoreServices:
            # PYTHON 3.5 ONLY - proc = sp.run(['sudo', 'systemctl', 'stop', service], stdout=sp.PIPE)
            p = sp.Popen(['sudo', 'systemctl', 'start', service], stdout=sp.PIPE, stderr=sp.PIPE)
            out, err = p.communicate()
            rc = p.returncode
            logger.info("\tret=%s out: %s, err: %s", rc, out.decode('utf-8'), err.decode('utf-8'))


class HostDiskLoad(HostFailure):

    TestFiles = {}

    def __init__(self, duration, read_ratio=100, random_ratio=0, n_threads=1, delay=0, ref_path='.'):
        super().__init__()
        self._duration = duration
        self._read_ratio = read_ratio
        self._random_ratio = random_ratio
        self._ref_path = ref_path
        self._dev_path = self.get_device_path(ref_path)
        self._n_threads = n_threads or cpu_count()

        self._delay = delay

        # check if test file exists
        self._test_file_path = self.TestFiles.setdefault(
            self._dev_path,
            os.path.abspath(os.path.join(self._ref_path, TestFileName))
        )

    @staticmethod
    def get_device_path(path):

        dev = os.stat(path)[ST_DEV]
        major = os.major(dev)
        minor = os.minor(dev)

        out = sp.Popen(
            shlex.split("find /sys/dev/ -name {}:{}".format(major, minor)),
            stdout=sp.PIPE
        ).communicate()
        if out:
            disk_folder = str(out[0], 'utf-8').strip()
            attrs = {}
            with open("{}/uevent".format(disk_folder), 'r') as fp:
                for line in fp:
                    key, value = line.strip().split('=')
                    attrs[key] = value
            dev = attrs.get('DEVNAME')
            if dev:
                dev = "/dev/{}".format(dev)
                if os.path.exists(dev):
                    return dev
        return None

    def trigger(self):
        self._scheduler.schedule(
            0,
            self.execute
        )

    def execute(self):
        logger.info("Triggering host disk failure")
        if not os.path.exists(self._test_file_path):
            out, err = sp.Popen(
                shlex.split("dd if=/dev/urandom of={} bs={} count={}".format(
                    self._test_file_path,
                    TestFileSize[0],
                    TestFileSize[1]
                )),
                stdout=sp.PIPE,
                stderr=sp.PIPE
            ).communicate()
            if os.path.exists(self._test_file_path):
                logger.debug("File created: %s", self._test_file_path)
            else:
                logger.error("Error creating file: out=%s, err=%s", out, err)

        if not os.path.exists(self._test_file_path):
            logger.error("Unable to access test file.")
            return

        bin_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'bin', 'btest'))
        cmd = "{} -t {} -T {} -j {} {} {} {}".format(
            bin_path,
            self._duration,
            self._n_threads,
            self._delay,
            int(self._random_ratio),
            int(self._read_ratio),
            self._test_file_path
        )
        logger.info("Generate disk failure: cmd=%s", cmd)

        out = sp.Popen(shlex.split(cmd), stdout=sp.PIPE).communicate()
        if out:
            logger.debug("Output of failure script: \n%s", '\n'.join([str(o, 'utf-8') for o in out if o]))
