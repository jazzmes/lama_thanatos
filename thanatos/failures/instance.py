import logging
from thanatos.failures.base import Failure
from thanatos.deps.libvirt import LibvirtConnector

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

logger = logging.getLogger(__name__)


# noinspection PyAbstractClass
class InstanceFailure(Failure):

    _hypervisor = LibvirtConnector()

    def __init__(self, instance_name):
        if not self._hypervisor.connected:
            self._hypervisor.connect()

        self._instance = self._hypervisor.get_instance(instance_name)
        logger.debug("Instance: %s", self._instance)


class InstanceCrash(InstanceFailure):

    def __init__(self, instance_name, duration=0):
        super().__init__(instance_name)
        self._duration = duration

    def trigger(self):
        self._scheduler.schedule(
            0,
            self._instance.abrupt_shutdown
        )
        if self._duration:
            self._scheduler.schedule(
                self._duration,
                self._instance.start
            )
