import traceback
from argparse import ArgumentParser
from datetime import timedelta
from logging import StreamHandler
from flask import make_response, request, current_app, Flask, logging, jsonify, json
from functools import update_wrapper

from thanatos.failures.host import HostDiskLoad, HostCrash
from thanatos.failures.instance import InstanceCrash

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

logger = logging.getLogger(__name__)

DefaultPort = 58229


def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):

    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, str):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, str):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers
            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            h['Access-Control-Allow-Credentials'] = 'true'
            h['Access-Control-Allow-Headers'] = \
                "Origin, X-Requested-With, Content-Type, Accept, Authorization"
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)

    return decorator


def start_event_server(port):

    app = Flask(__name__)

    @app.route('/fail/instance/<string:instance_name>', methods=['POST'])
    @crossdomain(origin='*')
    def fail_instance(instance_name):
        try:
            args = request.get_json() or {}
            duration = args.get('duration', 0)
            logger.debug("Will trigger %ss failure for instance: %s", duration, instance_name)

            InstanceCrash(instance_name, duration).trigger()
            return jsonify({'result': True})
        except Exception as e:
            logger.error("Error generation failure. Tb: %s", traceback.format_exc())
            return jsonify({'result': False})

    @app.route('/fail/host', methods=['POST'])
    @crossdomain(origin='*')
    def fail_host():
        try:
            args = request.get_json() or {}
            duration = args.get('duration', 0)
            logger.debug("Will trigger %ss host failure", duration)

            HostCrash(duration).trigger()
            return jsonify({'result': True})
        except Exception as e:
            logger.error("Error generation failure. Tb: %s", traceback.format_exc())
            return jsonify({'result': False})

    @app.route('/fail/host/disk', methods=['POST'])
    @crossdomain(origin='*')
    def fail_host_disk():
        try:
            args = request.get_json() or {}
            duration = int(args.get('duration', 10))
            read_ratio = int(args.get('read_ratio', 100))
            random_ratio = int(args.get('random_ratio', 0))
            ref_path = args.get('ref_path', '.')
            n_threads = args.get('n_threads', 1)
            delay = int(args.get('delay', 0))

            logger.debug("Host disk load: d=%s, Rd=%s, Rnd=%s, #Thds=%s, Delay=%s, Path=%s",
                         duration, read_ratio, random_ratio, n_threads, delay, ref_path)

            HostDiskLoad(duration, read_ratio, random_ratio, n_threads, delay, ref_path).trigger()
            return jsonify({'result': True})
        except Exception as e:
            logger.error("Error generation failure. Tb: %s", traceback.format_exc())
            return jsonify({'result': False})

    # Start the server
    logger.info("Start server on port: %s", port)
    app.run(host='0.0.0.0', port=port)


def get_stream_handler():
    sh = StreamHandler()
    sh.setFormatter(
        logging.Formatter(
            "%(asctime)s : %(levelname)5.5s : %(module)14.14s::%(funcName)20.20s[%(lineno)3.3s] : %(message)s"
        )
    )
    return sh


def main():
    argparse = ArgumentParser()
    argparse.add_argument("-p", dest="port",
                          help="Port to start http interface",
                          type=int, default=DefaultPort)
    argparse.add_argument("-l", dest="log_level", help="Log level", default="DEBUG",
                          choices=("NOTSET", "DEBUG", "INFO", "WARNING", "ERROR"))
    pargs = argparse.parse_args()

    # logger.addHandler(sh)
    # logger.setLevel(pargs.log_level)
    root_logger = logging.getLogger('')
    root_logger.addHandler(get_stream_handler())
    root_logger.setLevel(pargs.log_level)

    start_event_server(pargs.port)

if __name__ == "__main__":
    main()
