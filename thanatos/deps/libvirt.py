import libvirt
import traceback
from flask import logging

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger(__name__)


class LibvirtException(Exception):
    pass


class LibvirtConnector(object):

    def __init__(self):
        self.__conn = None

    def connect(self):
        self.__conn = libvirt.open('qemu:///system')

        if not self.__conn:
            raise LibvirtException("Failed to open connection to the hypervisor")

    def close(self):
        if self.__conn:
            self.__conn.close()

    @property
    def connected(self):
        return self.__conn is not None

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            logger.debug("Exception handling at exit of connector: type=%s, val=%s, tb=%s", exc_type, exc_val, exc_tb)
        self.close()

    def get_instance(self, instance_name):
        try:
            instance = self.__conn.lookupByName(instance_name)
            return Instance(instance)
        except libvirt.libvirtError as e:
            raise LibvirtException("Failed to retrieve instance '{}': {}".format(
                instance_name,
                e
            ))

    def list_instances(self):
        try:
            instance_ids = self.__conn.listDomainsID()
            logger.debug("Instance IDs: %s", instance_ids)
            instances = []
            for instance_id in instance_ids:
                try:
                    instance = Instance(self.__conn.lookupByID(instance_id))
                    if instance.name.startswith('lama'):
                        instances.append(instance)
                    else:
                        logger.debug("Ignored instance: %s", instance.name)
                except:
                    logger.debug("Unable to find instance with id: %s", instance_id)
                    logger.debug("Exception: %s", traceback.format_exc())

            return instances
        except libvirt.libvirtError as e:
            raise LibvirtException("Failed to list instances: {}".format(e))


class Instance(object):

    def __init__(self, libvirt_instance):
        self.__inner_instance = libvirt_instance

    @property
    def name(self):
        return self.__inner_instance.name()

    def abrupt_shutdown(self):
        rd = self.__inner_instance.destroy()
        ru = self.__inner_instance.undefine()
        logger.debug("Instance ID: %s, retdetroy=%s retundef=%s", self.__inner_instance.ID(), rd, ru)
